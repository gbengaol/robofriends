import React from 'react';

const Card = (props) => {
	return(
		<div style={CardStyle} >
			<img src={`https://robohash.org/${props.id}?size=200x200`} alt="Robot" />
			<div>
				<h1>{props.name}</h1>
				<p>{props.email}</p>
			</div>
		</div>
	);
}

const CardStyle = { 
	background: 'linear-gradient(135deg, seagreen, lightblue)', 
	color: 'red', 
	width: '20%', 
	display:'inline-block', 
	margin: '20px', 
	padding: '5px', 
	border: '2px solid red', 
	borderRadius:'20px', 
	textAlign: 'center',
	height: '500px',
	overflowY: 'auto'
}

export default Card;