import React from 'react';

const SearchBox = ({searchChange}) => {
	return(
			<div>
				<input 
					style={inputStyle} 
					type="search" 
					id="searchbox" 
					placeholder="Search for a Robot here"
					onChange={searchChange}
					autoFocus="autofocus"
				/>
			</div>
		);
}

const inputStyle = {
	height: '40px',
	width: '20%',
	border: '2px solid red',
	fontWeight: 'bold',
	boxShadow: 'none',
	textAlign: 'center',
	textTransform: 'capitalize',
	borderRadius: '3vw'
}

export default SearchBox;