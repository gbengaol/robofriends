import React from 'react';

const Register = ({usernameChange, passwordChange, registerbutton}) => {
	return(
			<div id="login-card">
				<input type="text" placeholder="Enter your username" onChange={usernameChange} /> <br/><br />
				<input type="text" placeholder="Enter your password" onChange={passwordChange} /> <br /><br />
				<button type="input" onClick={() => registerbutton('register')}>Register</button><br/><br/>
			</div>
		);
}


export default Register;