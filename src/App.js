import React from 'react';
import RobotsList from './RobotsList';
import SearchBox from './SearchBox.js';
import FormHeader from './FormHeader.js';
import LoginForm from './LoginForm.js';
import Register from './Register.js';
import Navbar from './Navbar.js';
import Scroll from './Scroll';

class App extends React.Component{
	constructor(){
		super();
		this.state = {
			robots: [],
			searchField: '',
			username: '',
			password: '',
			route: 'login',
			lim: 10
		}
	}

	componentDidMount (){		
		fetch (`https://jsonplaceholder.typicode.com/comments?_limit=${this.state.lim}`)
			.then(response => response.json())
			.then(users => this.setState({robots: users}));
	}

	moreRobots = async () => {
		await this.setState({lim: this.state.lim + 10});
		if (this.state.lim >= 200){
			alert('Limit has been reached');
		} else {
			console.log(this.state.lim);
		}
		this.componentDidMount();
	}

	onSearchChange = (event) => {
		this.setState({searchField: event.target.value})
	}

	usernameChange = (event) => {
		this.setState({username: event.target.value})
	}

	passwordChange = (event) => {
		this.setState({password: event.target.value})
	}

	loginbutton = () => {
		if(this.state.username !== '' && this.state.password !==''){
			this.setState({route: 'home'})
		} else{
			alert(' Fields cannot be empty')
			this.setState({route: 'login'})	
		}
	}
	registerbutton = (route) => {
		this.setState({route: route})
	}

	LoginFormRender = (route) => {
		this.setState({route: route})	
	}

	RegisterFormRender = (route) => {
		this.setState({route: route})	
	}

	logOut = (route) => {
		this.setState({route: route})
	}

	render(){

		const filteredRobots = this.state.robots.filter(robots => {
			return robots.name.toLowerCase().includes(this.state.searchField.toLowerCase())
		});

		return(
			<div>
			<h1>Welcome to my RoboFriends App</h1>
			{

				this.state.route === 'login' ? 
					<div>
						<FormHeader LoginFormRender={this.LoginFormRender} RegisterFormRender={this.RegisterFormRender} />
						<LoginForm
							usernameChange={this.usernameChange} 
							passwordChange={this.passwordChange}
							loginbutton={this.loginbutton} 
						/>
					</div>
					: (this.state.route === 'register' ?
					<div>
						<FormHeader LoginFormRender={this.LoginFormRender} RegisterFormRender={this.RegisterFormRender} />
						<Register
							usernameChange={this.usernameChange} 
							passwordChange={this.passwordChange}
							registerbutton={this.registerbutton} 
						/>
					</div> :
					<div>
						<Navbar logOut={this.logOut} moreRobots={this.moreRobots} />
						<SearchBox searchChange={this.onSearchChange} />
						<Scroll>
							<RobotsList robots={filteredRobots} />
						</Scroll>
					</div>
					)
			}
			</div>
	);
	}
}

export default App;