import React from 'react';

const Navbar = ({logOut, moreRobots}) => {
	return(
			<div style={{height:'40px'}}>
				<p><button className="btn btn-danger" type="input" onClick={() => logOut('login')}>Log Out</button> || <button className="btn btn-success" type="input" onClick={moreRobots}>More Robots</button></p><br/><br/>
			</div>
		);
}


export default Navbar;