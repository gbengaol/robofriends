import React from 'react';

const FormHeader = ({LoginFormRender, RegisterFormRender}) => {
	return(
			<div>
				<ul>
					<li className="btn btn-success" onClick={() => LoginFormRender('login')}>Log in</li>
					<li className="btn btn-danger" onClick={() => RegisterFormRender('register')}>Register</li>
				</ul>
			</div>
		);
}


export default FormHeader;