import React from 'react';

const Scroll = (props) => {
	return(
			<div style={Scroll_style}>
				{props.children}
			</div>
		);
}

const Scroll_style = {
	overflowY: 'scroll',
	height: '65vh',
	marginTop: '2vh'
}

export default Scroll;