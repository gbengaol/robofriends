import React from 'react';

const LoginForm = ({usernameChange, passwordChange, loginbutton}) => {
	return(
			<div id="login-card">
				<input type="text" placeholder="Enter your username" onChange={usernameChange} /> <br/><br />
				<input type="text" placeholder="Enter your password" onChange={passwordChange} /> <br /><br />
				<button type="input" onClick={() => loginbutton('login')}>Log in</button><br/><br/>
			</div>
		);
}


export default LoginForm;